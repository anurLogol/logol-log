import React from 'react';

function Log(p) {
  const { log , no} = p
  let levelColor = 'bg-secondary';
  const timestamp = new Date(log.timestamp)

  const dateStr = new Intl.DateTimeFormat('en-US', {year: 'numeric', month: '2-digit',day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit'}).format(timestamp);

  let messageStr = log.message.length

  messageStr = log.message.substring(0,400) + '...'
// console.log("key....",key)
  if (
    log.level === 'error' ||
    log.level === 'crit' ||
    log.level === 'alert' ||
    log.level === 'emerg'
  )
    levelColor = 'bg-red';
  if (log.level === 'warn') levelColor = 'bg-yellow';
  if (log.level === 'debug') levelColor = 'main';

  return (
    <div key={log._id} className={`grid grid-cols-10 border-b-2 border-ltGray ${no%2==0 ? 'bg-white':'bg-slate'}`}>
      <p className='shrink-0 text-gray text-sm '>
        {dateStr}
      </p>
      <div className='flex justify-center items-start'>
        <p
          className={`${levelColor} text-white inline-block px-3 rounded-full text-sm my-1`}>
          {log.level}
        </p>
      </div>
      <p className='text-gray col-span-7 text-xs text-overflow: ellipsis '>{log.message.replace(/\n/g, '')}</p>
    </div>
  );
}

export default Log;
