const express = require('express');
const Router = express.Router();
const logController = require('../controllers/logM2fleetController');
const filterLogController = require('../controllers/filterLogController');


Router.post('/log/store', logController.storeLog, (req, res, next) => {
    const { logStored } = res.locals;

    return res.status(200).json({
        status: true,
        log: logStored,
        message: 'Successfully stored log into DB',
    });
});

Router.get('/log/pagination', logController.getAllLogsPagination, (req, res, next) => {
    const { allLogs } = res.locals;
    return res.status(200).json({
        status: true,
        all: allLogs,
    });
});


module.exports = Router;