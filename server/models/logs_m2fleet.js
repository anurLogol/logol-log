const mongoose = require('mongoose');
const mongoosePaginate = require("mongoose-paginate-v2");
const Schema = mongoose.Schema;



const logM2fleetSchema = new Schema({
  message: {
    type: Schema.Types.Mixed,
    required: true,
  },
  level: {
    type: String,
    required: true,
  },
  timestamp: {
    type: Date,
    required: true,
    default: Date.now,
  },
  id_reff: {
    type: String,
    required: true,
  },
  port: {
    type: String,
    required: true,
  },
});
logM2fleetSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('log_m2fleet', logM2fleetSchema);
