const mongoose = require('mongoose');
const mongoosePaginate = require("mongoose-paginate-v2");
const Schema = mongoose.Schema;



const logSchema = new Schema({
  message: {
    type: String,
    text: true,
    required: true,
  },
  level: {
    type: String,
    required: true,
  },
  timestamp: {
    type: Date,
    required: true,
    default: Date.now,
  },
  isBackend: {
    type: Boolean,
    required: true,
    default: true,
  },
});
logSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('log', logSchema);
