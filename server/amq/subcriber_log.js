var amqp = require('amqplib/callback_api');
const sendRabbitMQ = require('./rabbitMQPublisher');
const QUEUE_NAME_CREATE = 'logol_logs'

const Logs = require('../models/logs');

amqp.connect(`amqp://developer:developer@10.1.19.27:5672`, function (error0, connection) {
    if (error0) {
        console.log("error0....");
        throw error0;
    }

    sendRabbitMQ(QUEUE_NAME_CREATE, JSON.stringify({ init: 'init' }))
    setTimeout(function () {


        connection.createChannel(async function (error1, channel) {
            if (error1) {
                console.log("error1....");
                throw error1;
            }
            channel.consume(QUEUE_NAME_CREATE, async function (data) {
                console.log(`=================================== AMQ ${QUEUE_NAME_CREATE} =======================================`)

                let errorMessage;
                var dataAmq = JSON.parse(data.content.toString())
                console.log(' DATA FROM QUEUE : ', data.content.toString())
                const {level,timestamp,message} = dataAmq
                
                if(level){
                    var body = {
                        level,
                        isBackend :true,
                        message : JSON.stringify(message),
                        timestamp: new Date(timestamp)
                    }
                    const data = await Logs.create(body);
                    console.log("data log store : ",data)
                }

            }, {
                noAck: true
            })
        });

    }, 1000);
});