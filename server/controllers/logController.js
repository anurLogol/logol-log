const Logs = require('../models/logs');
const path = require('path');
const { checkLogBody } = require('../utils/auxiliaryFunctions');

const logController = {};

logController.storeLog = async (req, res, next) => {
  let body = checkLogBody(req.body);
  console.log('bodyyy', body);

  try {//1679458932 now
    const total = 80
    for (let index = 0; index < total; index++) {
      console.log("============ INSERT DATA =========> ",index)
      let msg = body.message
      msg.no = 'data-' + index;

      if (index%2==0) {
        body.level = 'error'
      }else{
        body.level = 'info'
      }
      console.log("body.level ",body.level);

      const data = await Logs.create({
        ...body,
        message: JSON.stringify(msg),
        timestamp: new Date(),
      });

    }

    res.locals.logStored = {sukses:true};
    return next();
  } catch (err) {
    next({ message: err.message });
  }
};

logController.getAllLogs = async (req, res, next) => {
  try {
    // await Logs.remove({timestamp:{$gt:1679458932000}});
    const datas =[]// await Logs.find({timestamp:{$lt:1679408574000}});
    const data = await Logs.find({});
    if (!data) {
      const dataError = {
        log: 'getAllLogs middleware: Error from db when query for log',
        status: 406,
        message: { err: 'An error occurred' },
      };
      return next(dataError);
    }
    res.locals.allLogs = data;
    return next();
  } catch (err) {
    return next(err);
  }
};

logController.getAllLogsPagination = async (req, res, next) => {
  try {
    const data = await Logs.paginate({}, {page:2 })
    if (!data) {
      const dataError = {
        log: 'getAllLogs middleware: Error from db when query for log',
        status: 406,
        message: { err: 'An error occurred' },
      };
      return next(dataError);
    }
    res.locals.allLogs = data;
    return next();
  } catch (err) {
    return next(err);
  }
};
module.exports = logController;
