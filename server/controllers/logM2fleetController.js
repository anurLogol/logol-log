const Logs = require('../models/logs_m2fleet');
const path = require('path');
const { checkLogBody } = require('../utils/auxiliaryFunctions');


const logController = {};

logController.storeLog = async (req, res, next) => {
    let body = checkLogBody(req.body);

    try {//1679458932 now
        const data = await Logs.create({
            ...body,
            timestamp: new Date(),
            port : body.port || 'NPCT1'
        });

        res.locals.logStored = { sukses: true };
        return next();
    } catch (err) {
        next({ message: err.message });
    }
};

logController.getAllLogsPagination = async (req, res, next) => {
    try {
        const data = await Logs.paginate({}, {})
        if (!data) {
            const dataError = {
                log: 'getAllLogs middleware: Error from db when query for log',
                status: 406,
                message: { err: 'An error occurred' },
            };
            return next(dataError);
        }
        res.locals.allLogs = data;
        return next();
    } catch (err) {
        return next(err);
    }
};

module.exports = logController;